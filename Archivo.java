/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resources;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import static javafx.application.Application.launch;

/**
 *
 * @author User1
 */
public class Archivo {

        
    public Archivo() {
    }
    
    
    public ArrayList<String[]> leer(String archivo) throws FileNotFoundException, IOException {
        ArrayList lista = new ArrayList();
        String cadena;
        FileReader f = new FileReader(archivo);
        try (BufferedReader b = new BufferedReader(f)) {
            while((cadena = b.readLine())!=null) {
                String[] datos = cadena.split(",");               
                lista.add(datos);
            }}
        return lista;
    }
    public void escribir(ArrayList<String[]> lista) {
        Scanner entrada = new Scanner(System.in);
        final String NEXT_LINE = "\n"; //delimitador para salto de linea
        String delim =","; //separador de campos
        String csvFile = "Archivos/ubicacion.txt";//nombre de archivo de salida
        try {
            try (FileWriter fw = new FileWriter(csvFile) //objeto filewriter
            ) {
                
                fw.write("");
                for(int i=0;i<lista.size()-1;i++){
                    String cadena="";
                    for(int j=0;j<lista.get(i).length-1;i++){
                        cadena=cadena+lista.get(i)[j]+",";
                    }
                    fw.append(cadena);
                    }   fw.flush();
                //cerrar archivo
            }
	} catch (IOException e) {
            // Error al crear el archivo, por ejemplo, el archivo
            // está actualmente abierto.
            e.printStackTrace();
	}
    }
    public void escribir2(ArrayList<String[]> lista) throws IOException{
        FileWriter fichero = null;
        PrintWriter pw = null;
        fichero = new FileWriter("Archivos/ubicacion2.txt");
        pw = new PrintWriter(fichero);
            for(int i=0;i<lista.size();i++){
                String cadena="";
                    for(int j=0;j<lista.get(i).length;j++){
                        if(j==12){
                            cadena=cadena+lista.get(i)[j];
                        }
                        else{
                            cadena=cadena+lista.get(i)[j]+",";
                        }
                    }    
                pw.println(cadena);    
            }   
        fichero.close();
    }
}
