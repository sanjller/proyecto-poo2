/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resources;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Pierina Pereira
 */
public class Enemigo {

    public Enemigo(){}
    
    
    public ImageView enemigo(String ubiEnemigo) throws FileNotFoundException, IOException{
        Archivo archivo= new Archivo();
        ArrayList lista= archivo.leer("Archivos/ubicacion2.txt");
        final FileInputStream inputbl1= new FileInputStream(ubiEnemigo);
    final Image enemigo_img = new Image(inputbl1);
    ImageView enemigo= new ImageView(enemigo_img);
    int i=(int)(Math.random()*10);
    int j=(int)(Math.random()*11);
    String[] list= (String[]) lista.get(i);
    if((list[j]).equals("0")) {
        enemigo= new ImageView(enemigo_img);
        enemigo.setFitHeight(32);
        enemigo.setFitWidth(32);
        enemigo.setTranslateX(i*32+30);
        enemigo.setTranslateY(j*32+70);
            
        }
//archivo.escribir(lista);
        return enemigo;
    }

    
			
	
}
