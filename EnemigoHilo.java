/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.TranslateTransition;
import javafx.scene.Group;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
/**
 *
 * @author Pierina Paulette
 */
public class EnemigoHilo implements Runnable {

    public EnemigoHilo(Enemigo enemigo, Group componente, boolean vida, ImageView personaje) {
        this.enemigo = enemigo;
        this.componente = componente;
        this.vida = vida;
        this.personaje = personaje;
    }

    
   
    

    @Override
    
    public void run() {
        vida=true;
        try {
            Archivo archivo= new Archivo();
            ArrayList<String[]> lista;
            ImageView enemigo1= enemigo.enemigo("image/Level01/enemigo.gif");
//            componente.add(enemigo1)
            while(vida){
                lista= archivo.leer("Archivos/ubicacion2.txt");
                
            if (!(enemigo1.getLayoutY() <= 0)) {
                        int y1 = (int) (enemigo1.getLayoutY()-32)/32;
                        int x1 = (int) (enemigo1.getLayoutX())/32;
                        if (lista.get(y1)[x1].equals("0")||lista.get(y1)[x1].equals("3")) {
                            enemigo1.setLayoutY(enemigo1.getLayoutY() - KEYBOARD_MOVEMENT_DELTA);
                        }
                    }
             if (!(enemigo1.getLayoutX() >= 353)) {
                        int y2 = (int) (enemigo1.getLayoutY())/32;
                        int x2 = (int) (enemigo1.getLayoutX()+32)/32;
                        if (lista.get(y2)[x2].equals("0")||lista.get(y2)[x2].equals("3")) {
                        enemigo1.setLayoutX(enemigo1.getLayoutX() + KEYBOARD_MOVEMENT_DELTA);
                        }
                        }
             if (!(enemigo1.getLayoutY() >= 293)) {
                        int y3 = (int) (enemigo1.getLayoutY()+32)/32;
                        int x3 = (int) (enemigo1.getLayoutX())/32;
                        if (lista.get(y3)[x3].equals("0")||lista.get(y3)[x3].equals("3")) {
                        enemigo1.setLayoutY(enemigo1.getLayoutY() + KEYBOARD_MOVEMENT_DELTA);
                        }
                    }
             if (!(enemigo1.getLayoutX() <= 0)) {
                        int y4 = (int) (enemigo1.getLayoutY())/32;
                        int x4 = (int) (enemigo1.getLayoutX()-32)/32;
                        if (lista.get(y4)[x4].equals("0")||lista.get(y4)[x4].equals("3")) {
                        enemigo1.setLayoutX(enemigo1.getLayoutX() - KEYBOARD_MOVEMENT_DELTA);
                        }
                    }
          Thread.sleep(40);
             
        }
            
        } catch (IOException ex) {
            Logger.getLogger(EnemigoHilo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(EnemigoHilo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
}
    private Enemigo enemigo;
    private Group componente;
    private boolean vida;
    private ImageView personaje;
    private static final int      KEYBOARD_MOVEMENT_DELTA = 32;
    private static final Duration TRANSLATE_DURATION = Duration.seconds(0.15);
    
}
