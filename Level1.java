/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pantallas;

import Resources.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author joshu_000
 */
public class Level1 extends Stage{
    private static final int      KEYBOARD_MOVEMENT_DELTA = 32;
    
    public Level1() throws IOException{
        try {
            final Group background = new Background().backg();
            final Group border = new Border().border();
            final Group bricksolid = new SolidBrick().solidbr();
            Group blocks = new Block().blocks();
            ImageView personaje = new Personaje().setPersonaje();
            ImageView bomba = new Bomba().setBomba();
            final Group groot= new Group(background,border,bricksolid,blocks,personaje,bomba);
            

            final Scene scene = new Scene(groot, 480, 450);
            moverPersonaje(scene, personaje, bomba);
            Enemigo enemigo= new Enemigo();
//            Runnable r= new EnemigoHilo(enemigo,groot, );

            
            
            this.setTitle("BOMBERMAN");
            this.setScene(scene);
            this.show();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Level1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void moverPersonaje(Scene scene, final ImageView Personaje, final ImageView Bomba) throws IOException {
        Archivo archivo= new Archivo();
        ArrayList<String[]> lista= archivo.leer("Archivos/ubicacion2.txt");
    scene.setOnKeyPressed((KeyEvent event) -> {
            switch (event.getCode()) {
                case UP:
                    if (!(Personaje.getLayoutY() <= 0)) {
                        int y1 = (int) (Personaje.getLayoutY()-32)/32;
                        int x1 = (int) (Personaje.getLayoutX())/32;
                        if (lista.get(y1)[x1].equals("0")||lista.get(y1)[x1].equals("3")) {
                            Personaje.setLayoutY(Personaje.getLayoutY() - KEYBOARD_MOVEMENT_DELTA);
                        }
                    }
                    break;
                case RIGHT:
                    if (!(Personaje.getLayoutX() >= 353)) {
                        int y2 = (int) (Personaje.getLayoutY())/32;
                        int x2 = (int) (Personaje.getLayoutX()+32)/32;
                        if (lista.get(y2)[x2].equals("0")||lista.get(y2)[x2].equals("3")) {
                        Personaje.setLayoutX(Personaje.getLayoutX() + KEYBOARD_MOVEMENT_DELTA);
                        }
                        }
                    break;
                case DOWN:
                    if (!(Personaje.getLayoutY() >= 293)) {
                        int y3 = (int) (Personaje.getLayoutY()+32)/32;
                        int x3 = (int) (Personaje.getLayoutX())/32;
                        if (lista.get(y3)[x3].equals("0")||lista.get(y3)[x3].equals("3")) {
                        Personaje.setLayoutY(Personaje.getLayoutY() + KEYBOARD_MOVEMENT_DELTA);
                        }
                    }
                    break;
                case LEFT:
                    if (!(Personaje.getLayoutX() <= 0)) {
                        int y4 = (int) (Personaje.getLayoutY())/32;
                        int x4 = (int) (Personaje.getLayoutX()-32)/32;
                        if (lista.get(y4)[x4].equals("0")||lista.get(y4)[x4].equals("3")) {
                        Personaje.setLayoutX(Personaje.getLayoutX() - KEYBOARD_MOVEMENT_DELTA);
                        }
                    }
                    break;
                case A:
                    Bomba.setVisible(true);
                    Bomba.setLayoutX(Personaje.getLayoutX()-470);
                    Bomba.setLayoutY(Personaje.getLayoutY()-430);
                    Timeline duration = new Timeline(new KeyFrame(
                    Duration.millis(2500),
                    action -> Bomba.setVisible(false)));
                    duration.play();
                    break;
            }
        });
  }
    
}
