/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resources;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Pierina Pereira
 */
public class Block {

    public Block(){}
    
    public Group blocks() throws FileNotFoundException, IOException{
        Archivo archivo= new Archivo();
        ArrayList lista= archivo.leer("Archivos/ubicacion.txt");
        final FileInputStream inputbl1= new FileInputStream("image/Level01/simpleblock01.png");
        

Group blocks = new Group();
final Image BLOCK_01 = new Image(inputbl1);
ImageView block= new ImageView(BLOCK_01);
int r = 0;
while (r < 30) {
    int i=(int)(Math.random()*10);
    int j=(int)(Math.random()*11);
    String[] list = (String[])lista.get(i);
    if((list[j]).equals("0")) {
        block= new ImageView(BLOCK_01);
        block.setFitHeight(32);
        block.setFitWidth(32);
        block.setTranslateX(i*32+30);
        block.setTranslateY(j*32+70);
        blocks.getChildren().add(block);
        String[] list2 = (String[])lista.get(j);
        list2[i]="1";
        r++;       
        }    
    }
for (int k = 0; k < 11; k++) {
             String[] list = (String[])lista.get(k);
        System.out.println(Arrays.toString(list));
        } 
archivo.escribir2(lista);
        return blocks;
    }
}
