package Resources;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Santiago Llerena
 */
public class Bomba {
    public Bomba(){}
    

    public ImageView setBomba() throws FileNotFoundException {
        final FileInputStream inputbomb= new FileInputStream("image/bomba/bomb.gif");
        final Image bomb = new Image(inputbomb);
        final ImageView bomba = new ImageView(bomb);
        bomba.setTranslateX(500);
        bomba.setTranslateY(500);
        bomba.setFitHeight(35);
        bomba.setFitWidth(35);
        
        return bomba;
    }

}
